* Description

Defence³ is a tower defense game project made for the purpose of learning the core features of the Unity engine and C#. The enemies AI uses a simple implementation of the BFS algorithm to find the shortest path to their target. Please note that the game is still in early active development  and only access to the project repo is possible at the moment. In it you need to place up to 5 towers in strategic locations to stop the enemies from reaching your base. Future features may include more levels and more enemy and tower varieties.

* Instructions

At the present moment you can only play or test the game through the Unity Engine. To do that simply clone the project to your PC and open it in Unity to play or mess around as you desire.